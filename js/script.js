function factorial(n) {
  var res = 1;
  for (var i = 1; i <= n; i++) {
    res *= i;
  }
  return res;
}

function factRec(n) {
  var res = 1;
  if (n == 1) {
    return 1;
  }
  else {
    return n * factRec(n-1);
  }
}


while (true) {
  var n = prompt("Enter the number factorial u wanna find:", "Exp.: 5");
  if (n <= 0) {
    continue;
  }
  else {
    alert(factorial(n));
    alert(factRec(n));
    break;
  }
}
